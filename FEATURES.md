# Features:

 - [X] File-viewing
 - [X] Scrolling
 - [X] Writing
 - [X] Prompting
 - [X] Go-to line
 - [X] Searching
 - [ ] Syntax highlighting
   - [ ] Rust
   - [ ] C(++)
   - [ ] Go
   - [ ] Markdown
 - [ ] Better tab support
 - [ ] Split view
