// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
extern crate termion;

pub struct Terminal {
    pub width:  u16,
    pub height: u16
}

impl Terminal {
    pub fn set_size(&mut self) {
        // Get the tuple area and set the width and height
        let area = termion::terminal_size().expect("Couldn't get terminal size.");
        self.width = area.0;
        self.height = area.1;
    }
}
