// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{fs, io::{stdin, Write}};

use termion::input::TermRead;

use crate::{cursor, keys, read, window};

pub fn run(screen2: &mut termion::screen::AlternateScreen<termion::raw::RawTerminal<std::io::Stdout>>, file2: String, file_name2: String, path2: String) {
    // Make a mutable path
    let mut path = path2;
    // Make a mutable file_name
    let mut file_name = file_name2;
    // Make a mutable file
    let mut file: String = file2;
    // This is so that when we split it up it doesn't just completely crash
    if file.is_empty() {
        file = "\n".to_string();
    }
    // For the bottom line prompts like save as and messages
    let mut prompt = Prompt { print: "".to_string(), p_type: ' ', old_pos: Some((0, 2)) };
    // Make a mutable screen
    let mut screen = screen2;
    // Render the file here
    let lines: Vec<&str> = file.split('\n').collect::<Vec<&str>>();
    window::render_file(&mut screen, &file, 0, 0, &file_name, 0, 2, 0, "");
    // Split up the file
    // set up the cursor
    let mut cursor = cursor::Cursor { xpos:        0,
                                      ypos:        3,
                                      line_offset: lines.len().to_string().chars().count() as u8 + 2,
                                      xpos_line:   0,
                                      ypos_line:   2,
                                      offset:      cursor::Offset { x: 0, y: 0 },
                                      lines:       (lines.len()) as u32,
                                      style:       0 };
    // render the cursor and refresh the screen
    cursor.render_cursor();
    screen.flush().unwrap();
    // Start the loop
    loop {
        // Setup stdin
        let stdin = stdin();

        // Get key presses
        for c in stdin.keys() {
            // Make a vector of lines.
            let lines: Vec<&str>;
            let docs = read::get_docs();
            let file3 = file.replace('\t', "    ");
            // Depending on the cursor mode, set up the thing "file"
            let mode = cursor.style;
            match mode {
                | 0 => {
                    // Command Mode
                    lines = file3.split('\n').collect::<Vec<&str>>();
                },
                | 1 => {
                    // (Xer)ink mode
                    lines = file3.split('\n').collect::<Vec<&str>>();
                },
                | 3 => {
                    // Docs mode
                    lines = docs.split('\n').collect::<Vec<&str>>();
                },
                | 4 => {
                    // Prompting
                    lines = file3.split('\n').collect::<Vec<&str>>();
                },
                | _ => {
                    // Idk, plugins down the line or random things that shouldn't happen
                    lines = file3.split('\n').collect::<Vec<&str>>();
                }
            }
            // Re-setup cursor
            cursor = cursor::Cursor { xpos:        cursor.xpos,
                                      ypos:        cursor.ypos,
                                      line_offset: lines.len().to_string().chars().count() as u8 + 2,
                                      xpos_line:   cursor.xpos_line,
                                      ypos_line:   cursor.ypos_line,
                                      offset:      cursor::Offset { x: cursor.offset.x, y: cursor.offset.y },
                                      lines:       (lines.len()) as u32,
                                      style:       mode };
            // Get the key output
            let output: keys::Output = keys::match_keys(c.unwrap(),
                                                        cursor.style,
                                                        cursor.clone(),
                                                        lines.clone(),
                                                        file.replace('\t', "    ").split('\n').collect::<Vec<&str>>().len() as u32,
                                                        prompt.print,
                                                        prompt.p_type,
                                                        prompt.old_pos);
            // Reset prompt cus clippy screams at me otherwise
            prompt.print = "".to_string();
            // Use the key outputs
            match output.cmdout {
                | "break" => break,
                | "write" => {
                    file = output.line.to_owned();
                },
                | "save" => {
                    if output.line == *"" {
                        save(path.to_string(), &file);
                    } else {
                        path = output.line.clone();
                        let v: Vec<&str> = path.split('/').collect();
                        file_name = v.last().unwrap().to_string();
                        save(path.clone(), &file);
                    }
                    prompt.print = format!("Wrote {} lines.", output.cursor.lines - 1);
                },
                | "save_as" => {
                    prompt.print = format!("Save to path: {}", &output.line);
                    prompt.p_type = 'a';
                },
                | "prompt" => {
                    prompt.print = output.line.to_string();
                },
                | "goto" => {
                    prompt.print = format!("Go to line: {}", &output.line);
                    prompt.p_type = 'g';
                },
                | "find" => {
                    prompt.print = format!("Find: {}", &output.line);
                    prompt.p_type = 'f';
                    if prompt.old_pos == None {
                        prompt.old_pos = Some((cursor.xpos_line, cursor.ypos_line));
                    }
                },
                | _ => {}
            }
            // If the cursor changed or something happened
            if cursor != output.cursor || output.cmdout != "cursor" {
                // If it's in docs mode, render the docs, otherwise, render the file
                if output.cursor.style == 3 {
                    cursor = output.cursor;
                    // Had a bug where it wouldn't be updated correctly so this fixes that.
                    cursor.verify_cursor_pos(&lines);
                    window::render_file(&mut screen, &docs, cursor.offset.x as u16, cursor.offset.y as u16, &file_name, cursor.xpos_line, cursor.ypos_line, cursor.style, &prompt.print[..]);
                } else {
                    cursor = output.cursor;
                    // Had a bug where it wouldn't be updated correctly so this fixes that.
                    cursor.verify_cursor_pos(&lines);
                    window::render_file(&mut screen, &file, cursor.offset.x as u16, cursor.offset.y as u16, &file_name, cursor.xpos_line, cursor.ypos_line, cursor.style, &prompt.print[..]);
                }
            }
            if output.cmdout == "prompt" || output.cmdout == "goto" || output.cmdout == "save_as" || output.cmdout == "find" {
                prompt.print = output.line.to_string();
            } else {
                prompt.old_pos = None;
            }

            // Render the cursor and flush the output
            cursor.render_cursor();
            screen.flush().unwrap();
        }
        break;
    }
    // Reset cursor style
    print!("\x1b[ q");
}

pub fn write(letter: char, cursor: &cursor::Cursor, lines: &mut Vec<&str>) -> String {
    // Get a slice of the line from the start, to the area where the new character is, concatenate
    // the new character and the slice, and then the rest of the string.
    let line: String = format!("{}{}{}", &lines[cursor.ypos_line as usize - 2][.. cursor.xpos_line as usize], letter, &lines[cursor.ypos_line as usize - 2][cursor.xpos_line as usize ..]);
    let mut file_new = lines[0].to_string();
    let mut i = 0;
    // Concatenate the lines into a string and return the string
    for c in lines {
        if i != 0 {
            if i != cursor.ypos_line - 2 {
                file_new = format!("{}\n{}", file_new, c);
            } else {
                file_new = format!("{}\n{}", file_new, line);
            }
        } else {
            // So it doesnt have an extra line at the top
            if i != cursor.ypos_line - 2 {
                // If this isn't the current line
                file_new = format!("{}", c);
            } else {
                // Otherwise add the current line
                file_new = format!("{}", line);
            }
        }
        i += 1;
    }
    file_new
}

pub fn delete(cursor: &cursor::Cursor, lines: &mut Vec<&str>) -> String {
    // If it's not the first line, because that can cause problems
    if cursor.ypos_line != 2 {
        // If it's the first character on a line, join this line and the one before it.
        if cursor.xpos_line == 0 {
            let mut file_new = lines[0].to_string();
            let mut i = 0;
            for c in lines {
                if i != 0 {
                    if i == cursor.ypos_line - 2 {
                        file_new = format!("{}{}", file_new, c);
                    } else {
                        file_new = format!("{}\n{}", file_new, c);
                    }
                }
                i += 1;
            }
            file_new
        } else {
            // Otherwise, grab a string of the line up to the backspaced area, and after it.
            let line: String = format!("{}{}",
                                       read::slice(0, cursor.xpos_line as usize - 1, lines[cursor.ypos_line as usize - 2].to_string()),
                                       read::slice(cursor.xpos_line as usize, 18446744073709551615, lines[cursor.ypos_line as usize - 2].to_string()));
            let mut file_new = lines[0].to_string();
            let mut i = 0;
            // Join it together, making sure not to add an extra `\n` at the top
            for c in lines {
                if i != 0 {
                    if i != cursor.ypos_line - 2 {
                        file_new = format!("{}\n{}", file_new, c);
                    } else {
                        file_new = format!("{}\n{}", file_new, line);
                    }
                } else {
                    if i != cursor.ypos_line - 2 {
                        file_new = format!("{}", c);
                    } else {
                        file_new = format!("{}", line);
                    }
                }
                i += 1;
            }
            // Return the new file content
            file_new
        }
    } else {
        // If it is the first line of the file
        if cursor.xpos_line != 0 {
            // If the cursor is not on the first character in the file, grab a slice to the
            // backspaced area and after it.
            let line: String = format!("{}{}",
                                       read::slice(0, cursor.xpos_line as usize - 1, lines[cursor.ypos_line as usize - 2].to_string()),
                                       read::slice(cursor.xpos_line as usize, 18446744073709551615, lines[cursor.ypos_line as usize - 2].to_string(),));
            let mut file_new = lines[0].to_string();
            let mut i = 0;
            // Join it all together and return it.
            for c in lines {
                if i != 0 {
                    if i != cursor.ypos_line - 2 {
                        file_new = format!("{}\n{}", file_new, c);
                    } else {
                        file_new = format!("{}\n{}", file_new, line);
                    }
                } else {
                    if i != cursor.ypos_line - 2 {
                        file_new = format!("{}", c);
                    } else {
                        file_new = format!("{}", line);
                    }
                }
                i += 1;
            }
            file_new
        } else {
            // If the cursor is the first character in the file, just join the file togethor and
            // return it
            let mut file_new = lines[0].to_string();
            let mut i = 0;
            for c in lines {
                if i != 0 {
                    file_new = format!("{}\n{}", file_new, c);
                }
                i += 1;
            }
            file_new
        }
    }
}

pub fn save(path: String, file: &String) {
    // I know fs::write exists but it crashed whenever I used it and this doesn't. Idk why, I'll
    // have to look into it...
    let mut file2 = fs::File::create(path).expect("could not create file");
    file2.write_all((*file).as_bytes()).expect("Could not write to file");
}

pub fn find(slice: String, lines: Vec<&str>, cursor2: cursor::Cursor) -> cursor::Cursor {
    // Make mutable cursor
    let mut cursor = cursor2;
    // So we know if we found it.
    let mut found = false;
    let pos_x = find_line(&slice[..], lines[cursor.ypos_line as usize - 2], cursor.xpos_line as usize);
    match pos_x {
        | None => {},
        | Some(a) => {
            found = true;
            cursor.xpos_line = a;
        }
    }
    for i in cursor.ypos_line as usize - 1 .. lines.len() {
        if found {
            break;
        }
        // pos_x not POSIX
        let pos_x = find_line(&slice[..], lines[i], 0);
        match pos_x {
            | None => {},
            | Some(a) => {
                found = true;
                cursor.xpos_line = a;
                cursor.ypos_line = i as u32 + 2;
                break;
            }
        }
    }
    if !found {
        for i in 0 .. cursor.ypos_line as usize - 2 {
            // pos_x not POSIX
            let pos_x = find_line(&slice[..], lines[i], 0);
            match pos_x {
                | None => {},
                | Some(a) => {
                    cursor.xpos_line = a;
                    cursor.ypos_line = i as u32 + 2;
                    break;
                }
            }
        }
    }
    cursor.clone()
}

fn find_line(slice: &str, line: &str, at_x: usize) -> Option<u32> {
    if !line.contains(slice) {
        None
    } else {
        let mut i = at_x;
        let mut j = 0;
        let mut return_type = None;
        while i < line.len() {
            if line[i .. i + 1] == slice[j .. j + 1] {
                j += 1;
                if j == slice.len() {
                    return_type = Some(i as u32 - j as u32 + 1);
                    break;
                }
            } else {
                j = 0;
            }
            i += 1;
        }
        return_type
    }
}

struct Prompt {
    // The print is what will be printed out
    pub print:   String,
    // The p_type is the type of prompt so we know how to handle it
    pub p_type:  char,
    // Old_pos is the last position before any action, mainly used for searching.
    pub old_pos: Option<(u32, u32)>
}
