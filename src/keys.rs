// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use termion::event::*;

use crate::{cursor, read, xerink};

pub fn match_keys(key: termion::event::Key, mode: u8, cursor: cursor::Cursor, lines: Vec<&str>, len: u32, prompt_line: String, prompt_type: char, old_pos: Option<(u32, u32)>) -> Output {
    let output = match mode {
        // 0 is command mode, 1 is Xerink mode, and 2 is highlight mode.
        // 3 is documentation mode.
        | 0 => command_mode(key, cursor, lines),
        | 1 => ink_mode(key, cursor, lines),
        | 3 => doc_mode(key, cursor, lines, len),
        | 4 => prompt(key, cursor, prompt_line, lines, prompt_type, old_pos),
        | _ => command_mode(key, cursor, lines)
    };
    output
}

fn command_mode(keys: termion::event::Key, cursor2: cursor::Cursor, lines: Vec<&str>) -> Output {
    // Cmdout is for stuff like saving and breaking which I cant do in here.
    let mut cmdout = "";
    // Just mutable cursor.
    let mut cursor = cursor2;
    match keys {
        | Key::Ctrl('q') | Key::Ctrl('Q') => cmdout = "break", // Quit
        | Key::Ctrl('g') | Key::Ctrl('G') => {
            cmdout = "goto";
            cursor.style = 4;
        },
        | Key::Ctrl('f') => {
            cmdout = "find";
            cursor.style = 4;
        },
        | Key::Ctrl('a') | Key::Ctrl('A') => {
            cmdout = "save_as";
            cursor.style = 4;
        },
        | Key::Right => {
            cmdout = "cursor"; // Move the cursor and notify that it's changing the cursor.
            cursor.change_cursor_pos(1, 0, lines)
        },
        | Key::Left => {
            cmdout = "cursor";
            cursor.change_cursor_pos(-1, 0, lines)
        },
        | Key::Up => {
            cmdout = "cursor";
            cursor.change_cursor_pos(0, -1, lines)
        },
        | Key::Down => {
            cmdout = "cursor";
            cursor.change_cursor_pos(0, 1, lines)
        },
        | Key::Char('l') => {
            cmdout = "cursor"; // Put it at the first character of the line.
            cursor.set_cursor_pos(0, cursor.ypos_line, lines)
        },
        | Key::Char('L') => {
            cmdout = "cursor"; // Just put it at the end of the line.
            cursor.set_cursor_pos(65535, cursor.ypos_line, lines)
        },
        | Key::Char('F') => {
            cmdout = "cursor"; // Put the ypos at the last line.
            cursor.set_cursor_pos(cursor.xpos_line, 65535, lines)
        },
        | Key::Char('f') => {
            cmdout = "cursor"; // Put the y pos at the first line.
            cursor.set_cursor_pos(cursor.xpos_line, 2, lines)
        },
        | Key::PageDown => {
            cmdout = "cursor"; // Same as 'F'
            cursor.set_cursor_pos(cursor.xpos_line, 65535, lines)
        },
        | Key::PageUp => {
            cmdout = "cursor"; // Same as 'f'
            cursor.set_cursor_pos(cursor.xpos_line, 2, lines)
        },
        | Key::Ctrl('d') | Key::Ctrl('D') => {
            // Switch to documentation.
            cmdout = "mode";
            cursor.style = 3; // Style represents the mode of the editor.
            cursor.line_offset = 4; // Line offset is preknown, just like the message.
            cursor.lines = 34;
            cursor.set_cursor_pos(0, 2, lines) // Set the cursor to the beginning of the file
        },
        | Key::Ctrl('x') | Key::Ctrl('X') => {
            // Switch to ink mode.
            cmdout = "mode";
            cursor.style = 1;
        },
        | Key::Ctrl('s') | Key::Ctrl('S') => {
            // Tell the editor to save.
            cmdout = "save";
        },
        | Key::Ctrl('h') | Key::Ctrl('H') => {
            // Switch to highlight mode, which doesn't exist right now, at least not really.
            cmdout = "mode";
            cursor.style = 2;
        },
        | _ => {}
    }
    let output = Output { cmdout, cursor, line: "".to_string() };
    output
}

fn doc_mode(keys: termion::event::Key, cursor2: cursor::Cursor, lines: Vec<&str>, len: u32) -> Output {
    let mut cmdout = "";
    let mut cursor = cursor2;
    match keys {
        | Key::Ctrl('q') | Key::Ctrl('Q') => cmdout = "break", // Quit
        | Key::Esc => {
            // Go back to the file and set the cursor to the beginning of the file.
            cmdout = "mode";
            cursor.style = 0;
            cursor.line_offset = (len - 1).to_string().chars().count() as u8 + 2;
            cursor.lines = len;
            cursor.set_cursor_pos(0, 2, lines)
        },
        | Key::Right => {
            // Move the cursor
            cmdout = "cursor";
            cursor.change_cursor_pos(1, 0, lines)
        },
        | Key::Left => {
            cmdout = "cursor";
            cursor.change_cursor_pos(-1, 0, lines)
        },
        | Key::Up => {
            cmdout = "cursor";
            cursor.change_cursor_pos(0, -1, lines)
        },
        | Key::Down => {
            cmdout = "cursor";
            cursor.change_cursor_pos(0, 1, lines)
        },
        | _ => {}
    }
    let output = Output { cmdout, cursor, line: "".to_string() };
    output
}

fn ink_mode(keys: termion::event::Key, cursor2: cursor::Cursor, lines: Vec<&str>) -> Output {
    // Make lines mutable.
    let mut lines2 = lines;
    // Make cursor mutable.
    let mut cursor = cursor2;
    let cmdout;
    // Line is for changes
    let line;
    // Char is still useless...
    match keys {
        | Key::Right => {
            // Move cursor pos
            cmdout = "cursor";
            cursor.change_cursor_pos(1, 0, lines2);
            line = "".to_string();
        },
        | Key::Left => {
            cmdout = "cursor";
            cursor.change_cursor_pos(-1, 0, lines2);
            line = "".to_string();
        },
        | Key::Up => {
            cmdout = "cursor";
            cursor.change_cursor_pos(0, -1, lines2);
            line = "".to_string();
        },
        | Key::Down => {
            cmdout = "cursor";
            cursor.change_cursor_pos(0, 1, lines2);
            line = "".to_string();
        },
        | Key::Char('\t') => {
            // Just edit it
            // As of now, tabs are just converted to spaces
            line = xerink::write('\t', &cursor, &mut lines2);
            cmdout = "write";
            // There was some ownership problems so I make another lines thats only used to fake
            // for the cursor
            let mut lines3 = lines2;
            // Make it the same length but + 4 cus spaces are 4 spaces.
            let line4 = &"a".repeat(5 + read::len(lines3[cursor.ypos_line as usize - 2].to_string().replace('\t', "    ")));
            // Update lines3 so the cursor won't be caught by the previous line length
            lines3[cursor.ypos_line as usize - 2] = line4;
            cursor.change_cursor_pos(4, 0, lines3);
        },
        | Key::Backspace => {
            // Delete
            line = xerink::delete(&cursor, &mut lines2);
            cmdout = "write";
            // Make it mutable.
            let mut lines3 = lines2;
            // Edit it
            let line4 = &"a".repeat(2 + read::len(lines3[cursor.ypos_line as usize - 2].to_string().replace('\t', "    ")));
            // Update it
            lines3[cursor.ypos_line as usize - 2] = line4;
            // Move the cursor based on what you remove. If it's a '\n' move to the current end of
            // the line, otherwise move back.
            if cursor.xpos_line == 0 && cursor.ypos_line != 2 {
                cursor.set_cursor_pos(65535, cursor.ypos_line - 1, lines3);
                if cursor.lines > 0 {
                    cursor.lines -= 1;
                    cursor.line_offset = cursor.lines.to_string().chars().count() as u8 + 2;
                }
                if cursor.offset.y != 0 {
                    cursor.offset.y -= 1;
                }
            } else {
                cursor.change_cursor_pos(-1, 0, lines3);
            }
        },
        | Key::Char(c) => {
            // Make the new line
            line = xerink::write(c, &cursor, &mut lines2);
            cmdout = "write";
            // If it's enter, add another line, update the line_offset, and move the cursor down
            if c == '\n' {
                cursor.lines += 1;
                cursor.line_offset = cursor.lines.to_string().chars().count() as u8 + 2;
                cursor.set_cursor_pos(0, cursor.ypos_line + 1, lines2);
            } else {
                // Otherwise just update the line, and move the cursor one to the right with the
                // updated lines.
                let mut lines3 = lines2;
                let line4 = &"a".repeat(2 + read::len(lines3[cursor.ypos_line as usize - 2].to_string().replace('\t', "    ")));
                lines3[cursor.ypos_line as usize - 2] = line4;
                cursor.change_cursor_pos(1, 0, lines3);
            }
        },
        | Key::Esc => {
            // Switch back to Command Mode.
            cmdout = "";
            cursor.style = 0;
            line = "".to_string()
        },
        | _ => {
            cmdout = "";
            line = "".to_string();
        }
    }
    Output { cmdout, cursor, line }
}

fn prompt(keys: termion::event::Key, cursor2: cursor::Cursor, line2: String, lines: Vec<&str>, prompt_type: char, old_pos: Option<(u32, u32)>) -> Output {
    // Match the output to decide what to prompt and do
    let mut cmdout = match prompt_type {
        | 'g' => "goto",
        | 'f' => "find",
        | 'a' => "save_as",
        | 'p' => "prompt",
        | _ => "prompt"
    };
    // Make mutable cursor
    let mut cursor = cursor2;
    let line = match keys {
        // If it's escape cancel everything and revert back to the previous state
        | Key::Esc => {
            cmdout = "mode";
            cursor.style = 0;
            match old_pos {
                | Some((a, b)) => {
                    cursor.xpos_line = a;
                    cursor.ypos_line = b;
                },
                | None => {}
            }
            line2
        },
        // If the key being pressed is enter, process the input
        | Key::Char('\n') => {
            // Go back to command mode
            cursor.style = 0;
            match prompt_type {
                | 'g' => match line2.parse::<u32>() {
                    | Ok(a) => {
                        cursor.set_cursor_pos(cursor.xpos_line, a + 1, lines);
                        line2
                    },
                    | Err(e) => {
                        // This is to display the error
                        cmdout = "prompt";
                        format!("Err: {}", e)
                    }
                },
                | 'a' => {
                    cmdout = "save";
                    line2
                },
                | 'f' => {
                    cmdout = "find";
                    if !line2.is_empty() {
                        cursor = crate::xerink::find(line2.to_owned(), lines.clone(), cursor);
                        cursor.verify_cursor_pos(&lines);
                    }
                    line2
                },
                // Just feed in the current thing and dont interact with it
                | _ => line2
            }
        },
        | Key::Char(a) => {
            if prompt_type == 'f' {
                cursor = crate::xerink::find(format!("{}{}", line2, a), lines.clone(), cursor);
                cursor.verify_cursor_pos(&lines);
            }
            format!("{}{}", line2, a)
        },
        | Key::Backspace => read::slice(0, read::len(line2.to_owned()) - 1, line2.to_owned()),
        | Key::Down => {
            if prompt_type == 'f' {
                let len = read::len(line2.clone());
                cursor.xpos_line += len as u32;
                cursor.verify_cursor_pos(&lines);
                let cx = cursor.xpos_line;
                cursor = crate::xerink::find(line2.clone(), lines.clone(), cursor);
                if cx == cursor.xpos_line {
                    cursor.xpos_line -= len as u32;
                }
                cursor.verify_cursor_pos(&lines);
            }
            line2
        },
        | _ => line2
    };
    Output { cmdout, cursor, line }
}

pub struct Output {
    pub cmdout: &'static str,
    pub cursor: cursor::Cursor,
    pub line:   String
}
