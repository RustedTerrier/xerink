// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use crate::{read, terminal};

#[derive(PartialEq, Copy, Clone)]
pub struct Cursor {
    pub xpos:        u16,
    pub ypos:        u16,
    pub line_offset: u8,
    pub lines:       u32,
    pub xpos_line:   u32,
    pub ypos_line:   u32,
    pub offset:      Offset,
    // Was originally just going to be the cursor style but is now the editor mode
    pub style:       u8
}

// Why did I make an entire struct for two values? Idk, organization maybe? Best excuse I could
// come up with...
#[derive(PartialEq, Copy, Clone)]
pub struct Offset {
    pub x: u32,
    pub y: u32
}
// Setting it to 4 broke some stuff
const EDITOR_OFFSET: u8 = 4;

impl Cursor {
    pub fn change_cursor_pos(&mut self, xchange: i16, ychange: i16, lines: Vec<&str>) {
        // Make a thing of the new possible xposition and make sure it's valid
        let mut x = self.xpos_line as i64 + xchange as i64;
        if x < 0 {
            x = 0;
        }
        self.xpos_line = x as u32;
        // Make a thing of the new possible yposition and make sure it's valid
        let mut y = self.ypos_line as i64 + ychange as i64;
        if y < 2 {
            y = 2;
        }
        if y > (self.lines) as i64 {
            y = self.lines as i64;
            self.offset.y -= 1;
        }
        self.ypos_line = y as u32;
        // Check if scrolling is needed
        self.scroll(xchange, ychange, &lines);
        // Make sure everything is good
        self.verify_cursor_pos(&lines);
    }

    pub fn render_cursor(&mut self) {
        self.set_real_cursor_pos();
        // Based on the mode, change the cursor
        match self.style {
            | 1 => print!("{}", termion::cursor::BlinkingBar),
            | _ => print!("{}", termion::cursor::SteadyBlock)
        }
        // Render the cursor in it's position
        print!("{}", termion::cursor::Goto(self.xpos + self.line_offset as u16, self.ypos));
    }

    pub fn set_real_cursor_pos(&mut self) {
        self.ypos = self.ypos_line as u16 - self.offset.y as u16;
        self.xpos = self.xpos_line as u16 - self.offset.x as u16;
    }

    pub fn set_cursor_pos(&mut self, x: u32, y: u32, lines: Vec<&str>) {
        self.xpos_line = x;
        self.ypos_line = y;
        if x == 0 {
            self.offset.x = 0;
        }
        if y == 2 {
            self.offset.y = 0;
        }
        self.verify_cursor_pos(&lines);
    }

    pub fn verify_cursor_pos(&mut self, lines: &Vec<&str>) {
        let mut size = terminal::Terminal { width: 0, height: 0 };
        size.set_size();
        if self.ypos_line > self.lines as u32 {
            self.ypos_line = self.lines as u32;
        }
        if self.offset.y > self.ypos_line {
            self.offset.y = self.ypos_line - size.height as u32 + (EDITOR_OFFSET as u16 - 1) as u32;
        }
        if self.ypos_line > self.offset.y + (size.height - (EDITOR_OFFSET as u16 - 1)) as u32 {
            self.offset.y = self.ypos_line - size.height as u32 + (EDITOR_OFFSET - 1) as u32;
        }
        if self.offset.y > self.lines as u32 {
            self.offset.y = 0;
        }
        let len_of_line = read::len(lines[(self.ypos_line - (EDITOR_OFFSET as u32 - 3) - 1) as usize].replace('\t', "    ")) as u32;

        if self.xpos_line > len_of_line {
            self.xpos_line = len_of_line;
            if self.xpos_line > size.width as u32 - self.offset.x as u32 - self.line_offset as u32 {
                self.offset.x = self.xpos_line - size.width as u32 - self.line_offset as u32;
            }
        }

        if self.offset.x > self.xpos_line {
            self.offset.x = self.xpos_line - (size.width - self.line_offset as u16) as u32;
            if 0 > self.xpos_line as i64 - (size.width - self.line_offset as u16) as i64 {
                self.offset.x = 0;
            }
        }

        if self.xpos_line > self.offset.x + (size.width - self.line_offset as u16) as u32 {
            self.offset.x = self.xpos_line - (size.width - self.line_offset as u16) as u32;
        }
    }

    pub fn scroll(&mut self, xchange: i16, ychange: i16, lines: &Vec<&str>) {
        // The folloing code is not for the faint of heart...
        let mut size = terminal::Terminal { width: 0, height: 0 };
        size.set_size();
        if self.xpos == size.width - self.line_offset as u16 && xchange > 0 {
            self.offset.x += 1;
        }
        if self.xpos == 0 && xchange < 0 {
            self.offset.x -= 1;
            if self.offset.x >= 65534 {
                self.offset.x += 1;
            }
        }
        if self.ypos == 2 && ychange < 0 {
            if self.offset.y >= 1 {
                self.offset.y -= 1;
            }
        }
        if self.ypos == size.height - (EDITOR_OFFSET as u16 - 1) && ychange > 0 {
            self.offset.y += 1;
        }
        self.verify_cursor_pos(&lines);
    }
}
