// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{fs, path::Path};
extern crate unicode_segmentation;

use unicode_segmentation::UnicodeSegmentation;

pub fn read_file(path: String) -> String {
    let file;
    // If the path doesn't exist, give a welcome message.
    if !&path[..].is_empty() {
        if Path::new(&path).exists() {
            // Read the file and return it
            file = fs::read_to_string(&path).expect("Couldn't read file.");
        } else {
            // Make the file and an empty string
            fs::write(&path, "").expect("Couldn't create file");
            file = "".to_string();
        }
    } else {
        file = "Welcome to Xerink!\n".to_string();
    }
    file
}

pub fn get_file_name(path: String) -> String {
    // Remove the directories before it so you only have the filename
    if path.contains('/') {
        let directories: Vec<&str> = path.split("/").collect();
        directories[directories.len() - 1].to_string()
    } else {
        if !&path[..].is_empty() {
            // If there is no directories before it, return it as it is
            path
        } else {
            // If the path is empty
            "Welcome!".to_string()
        }
    }
}

pub fn get_docs() -> String {
    // Will be reworked later...
    "Welcome to XERINK!

XERINK aims to be a feature rich text editor, built entirely in rust.

Commands:
The control key is represented by a '^', so ^C would be control + C.
Any commands that use modifier keys, such as control, will not be case-sensitive.

Modes:
The default mode for XERINK is command mode.
Command mode does not allow printing text but instead a vast use of keybindings.
Ink mode allows for printing to a file.
Highlight mode allows for highlighting text and then copying or deleting the selected text.

Common Commands:

Command Mode:
'l' => Jump to the beginning of a line.
'L' => Jump to the end of a line.
'f' => Jump to the beginning of a file.
'F' => Jump to the end of a file.
^Q  => Quit.
^A  => Save to path.
^S  => Save changes.
^D  => Switch to the documentation.
^G  => Go to line.
^H  => Switch to Highlight mode.
^X  => Switch to (XER)Ink mode.

Ink Mode:
ESC => Switch to Command Mode.

Highlight Mode:
Arrow Keys => Change direction.
'C'        => Copy selected text.
'E'        => Eliminate selected text.
ESC        => Switch to Command mode.

Docs Mode:
ESC => Switch to Command Mode.
^Q  => Quit.
        ".to_string()
}

pub fn len(s: String) -> usize {
    // Replace tabs with 4 spaces and get the graphemes
    let s = s.replace('\t', "    ");
    let graphemes = UnicodeSegmentation::graphemes(&s[..], true).collect::<Vec<&str>>();
    graphemes.len()
}

pub fn slice(start: usize, end: usize, s: String) -> String {
    let s = s.replace('\t', "    ");
    let graphemes = UnicodeSegmentation::graphemes(&s[..], true).collect::<Vec<&str>>();
    let mut file2 = "".to_string();
    let mut i = 0;
    if start < end {
        for c in graphemes {
            if i >= start && i < end {
                file2 = format!("{}{}", file2, c);
            }
            i += 1;
        }
    }
    file2
}
