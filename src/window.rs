// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
extern crate termion;

use std::io::{stdout, Write};

use termion::{color, raw::IntoRawMode, screen::*};

use crate::{read, terminal};

const EDITOR_OFFSET: u8 = 4; // This is how many lines aren't taken up by the file.

// This is a long af comment that has to be super long and be fast the current screen I have right now to see how far this goes right now and thats fun really really fun fun fun

pub fn alt_screen() -> termion::screen::AlternateScreen<termion::raw::RawTerminal<std::io::Stdout>> {
    // Move to alternate screen
    let mut screen = AlternateScreen::from(stdout().into_raw_mode().unwrap());
    // Switch to the alternate screen and make the cursor a blinky box.
    write!(screen, "{}{}{}", ToAlternateScreen, termion::cursor::BlinkingBlock, termion::cursor::Goto(0, 1)).unwrap();
    screen
}

pub fn clear_screen() {
    // Pretty self explanatory
    print!("\x1b[2J\x1b[H");
}

pub fn render_file(screen: &mut termion::screen::AlternateScreen<termion::raw::RawTerminal<std::io::Stdout>>, file: &String, xoffset: u16, yoffset: u16, file_name: &String, xpos: u32, ypos: u32,
                   mode: u8, prompt: &str) {
    let lines: Vec<&str>;
    lines = file.split('\n').collect::<Vec<&str>>();
    let mut real_lines: Vec<String> = Vec::new();
    // For the line number
    let spaces = " ".repeat(lines.len().to_string().len() - 1);
    // Get terminal size
    let mut size = terminal::Terminal { width: 0, height: 0 };
    size.set_size();
    let mut c = yoffset;
    while c < size.height - (EDITOR_OFFSET - 1) as u16 + yoffset && c as usize != lines.len() {
        // Thing is the current line to be added.
        let thing: String;
        // Spaces here are the spaces to be used after the line number.
        let spaces_here = &spaces[0 .. (&spaces.len() + 1) - ((c + 1).to_string().len())];
        // If it's the current line, make the line number background yellow
        let color;
        if ypos as usize - 1 == c as usize + 1 {
            color = format!("{}{}", color::Fg(color::Black), color::Bg(color::Yellow));
        } else {
            color = format!("{}{}", color::Fg(color::Black), color::Bg(color::LightWhite));
        }
        // If it's not the final line.
        if c as usize != lines.len() {
            // If it needs to be cut off, cut part of it off
            if read::len(lines[c as usize].to_string()) > xoffset as usize {
                thing = format!("{}{}{}{}{} {}",
                                color,
                                spaces_here,
                                c + 1,
                                color::Bg(color::Reset),
                                color::Fg(color::Reset),
                                read::slice(xoffset as usize, xoffset as usize + size.width as usize - (spaces.len() + 2), lines[c as usize].replace('\t', "    ")));
            } else {
                thing = format!("{}{}{}{}{}", color, spaces_here, c + 1, color::Bg(color::Reset), color::Fg(color::Reset));
            }
        } else {
            thing = format!("{} {}{}{}", color, spaces, color::Bg(color::Reset), color::Fg(color::Reset));
        }
        // Push the new line
        real_lines.push(thing.to_string());
        // Increment i the iterator
        c += 1;
    }
    // For some reason there is always an empty thing at the end so we get rid of that here.
    real_lines.pop();
    // If the file doesn't fill up the whole screen, add a blank wall, the size of the line offset
    // to the side.
    if (real_lines.len() as i32 + EDITOR_OFFSET as i32) < size.height as i32 {
        while (real_lines.len() as i32 + EDITOR_OFFSET as i32) < size.height as i32 {
            real_lines.push(format!("{}{} {}{}{}", color::Fg(color::Black), color::Bg(color::LightWhite), spaces, color::Bg(color::Reset), color::Fg(color::Reset)));
        }
    }

    let mut file2 = String::new();
    for c in &real_lines {
        file2 = format!("{}{}\r\n", file2, &c);
    }

    clear_screen();
    // Print out the top line
    write!(screen, "{}\r\n", get_top_line(&size)).unwrap();
    // Print out the file
    write!(screen, "{}", file2).unwrap();
    // Print out the bottom line
    write!(screen, "{}", get_bottom_line(&size, file_name, xpos, ypos, mode, prompt)).unwrap();
    // Print out the Commands at the bottom of the screen.
    // Ripping off nano? What? I don't even know what nano is...
    write_out_commands(screen);
}

fn write_out_commands(screen: &mut termion::screen::AlternateScreen<termion::raw::RawTerminal<std::io::Stdout>>) {
    // Just color the commands and write them out
    write!(screen, "  {}{}^D{}{} DOCS", color::Fg(color::Black), color::Bg(color::LightWhite), color::Bg(color::Reset), color::Fg(color::Reset)).unwrap();
    write!(screen, "  {}{}^X{}{} INK \n\r", color::Fg(color::Black), color::Bg(color::LightWhite), color::Bg(color::Reset), color::Fg(color::Reset)).unwrap();
    write!(screen, "  {}{}^Q{}{} QUIT", color::Fg(color::Black), color::Bg(color::LightWhite), color::Bg(color::Reset), color::Fg(color::Reset)).unwrap();
    write!(screen, "  {}{}^H{}{} HIGH", color::Fg(color::Black), color::Bg(color::LightWhite), color::Bg(color::Reset), color::Fg(color::Reset)).unwrap();
}

fn get_top_line(size: &terminal::Terminal) -> String {
    // Just get the version, join it with XERINK, color it and center it with spaces
    let version = env!("CARGO_PKG_VERSION");
    let mut top_line = format!("{}{}{}XERINK v{}{}",
                               color::Fg(color::Black),
                               color::Bg(color::LightWhite),
                               " ".repeat((size.width as usize - 8 - version.len()) / 2),
                               version,
                               " ".repeat((size.width as usize - 8 - version.len()) / 2));
    if read::len(version.to_string()) + 8 > size.height as usize {
        top_line = " ".repeat(size.width as usize).to_string();
    }
    if read::len(version.to_string()) % 2 != (size.width % 2) as usize {
        top_line = format!("{} ", top_line);
    }
    top_line
}

fn get_bottom_line(size: &terminal::Terminal, file_name: &String, xpos: u32, ypos: u32, mode: u8, prompt: &str) -> String {
    // Display the mode of the text editor in one character
    let xerink_mode = match mode {
        | 0 => format!("{} C ", color::Bg(color::Red)),
        | 1 => format!("{} X ", color::Bg(color::Green)),
        | 2 => format!("{} H ", color::Bg(color::Yellow)),
        | 3 => format!("{} D ", color::Bg(color::Blue)),
        // 4 is prompting and shouldn't be a real mode
        | 4 => format!("{} C ", color::Bg(color::Red)),
        | _ => format!("{} ? ", color::Bg(color::Magenta))
    };
    if prompt.is_empty() {
        // Print the informational bottom line
        let mut status_line = format!("{}|{}", xpos + 1, ypos - 1);

        // If the filename is too big for the terminal, dont show it
        let status_len = read::len((&status_line).to_string()) + read::len(file_name.to_owned());
        if status_len + 4 > size.height as usize {
            status_line = format!("{}|{}", xpos + 1, ypos - 1);
        }
        let status_len = read::len((&status_line).to_string()) + read::len(file_name.to_owned());
        // Center it
        status_line = format!("{}{}{}{}{}{}",
                              xerink_mode,
                              color::Bg(color::LightWhite),
                              " ".repeat(((size.width as usize - status_len) / 2) - 3),
                              file_name,
                              " ".repeat((size.width as usize - status_len) / 2),
                              status_line);
        // If it needs an extra space, add an extra space
        if read::len((&status_line).to_string()) % 2 == (size.width % 2) as usize {
            status_line = format!("{}|{}", xpos + 1, ypos - 1);
            status_line = format!("{}{}{}{}{} {}",
                                  xerink_mode,
                                  color::Bg(color::LightWhite),
                                  " ".repeat(((size.width as usize - status_len) / 2) - 3),
                                  file_name,
                                  " ".repeat((size.width as usize - status_len) / 2),
                                  status_line);
        }
        // Add colors
        format!("{}{}{}{}{}", color::Fg(color::Black), color::Bg(color::LightWhite), status_line, color::Fg(color::Reset), color::Bg(color::Reset))
    } else {
        format!("{}{}{} {}{}{}{}",
                color::Fg(color::Black),
                xerink_mode,
                color::Bg(color::LightWhite),
                prompt,
                " ".repeat(size.width as usize - (4 + read::len(prompt.to_string()))),
                color::Fg(color::Reset),
                color::Bg(color::Reset))
        // This won't be the real thing, this is just for promtping, such as going to a new line or searching.
    }
}
