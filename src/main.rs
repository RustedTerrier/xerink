// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
pub mod terminal;
pub mod cursor;
pub mod xerink;
pub mod keys;
mod window;
mod read;

use std::env::args;

fn main() {
    // Make an alternate screen
    let mut screen = window::alt_screen();
    // Start the editor with the file, filename, and path
    xerink::run(&mut screen, read::read_file(parse_args()), read::get_file_name(parse_args()), parse_args());
}

fn parse_args() -> String {
    // If the arg length is bigger than 1, take the first arg as the file. Should be improved
    // later...
    let args: Vec<String> = args().collect();
    let mut file = "";
    if args.len() >= 2 {
        file = &args[1][..];
    }
    file.to_string()
}
